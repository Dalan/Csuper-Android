Csuper Android
===============

![Csuper logo](app/src/main/res/mipmap-xhdpi/logo.png)


Csuper (Universal points counter allowing a dispense with reflection) for Android

Informations
------------

This software allow you to count points easily with a computer.
We can also save these games in a .csu file. (see documentation).

If you have any remarks or question on the use of csuper or on the code, don't hesitate to report to me.

You can find a lot of informations on [my website](http://www.dalan.rd-h.fr/wordpress).

The Android version of Csuper is currently not usable and is in heavy devellopment.

Installation
------------
### Executables
A apk file can be founded [here](http://www.dalan.rd-h.fr/binaries/Csuper-Android/latest.apk).

![QR code](Download-latest.png)

### Compilation
The code is build with Android Studio.


Additional Information
----------------------

If you have any remark, do not hesitate to open an issue.