/*
* MainActivity.java
*
* Copyright 2014-2016
* Remi BERTHO <remi.bertho@openmailbox.org>
*
* This file is part of Csuper.
*
* Csuper is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* Csuper is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
*
*
*/

package org.dalan.csuper.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import org.dalan.csuper.Core.Exception.XmlError;
import org.dalan.csuper.Core.Game;
import org.dalan.csuper.Core.Utility.I18n;
import org.dalan.csuper.R;

import java.io.File;

import ar.com.daidalos.afiledialog.FileChooserDialog;

public class MainActivity extends AppCompatActivity
		implements NavigationView.OnNavigationItemSelectedListener {

	///////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////// Attributes /////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * The game
	 */
	private Game game_;

	/**
	 * The file
	 */
	private File file_;

	private TextView text;

	///////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////// Creation functions /////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Called when the application is launch
	 * @param savedInstanceState
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);


		// I18n
		I18n.setContext(this);


		// Floating button
		FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
		fab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
						.setAction("Action", null).show();
			}
		});


		// Side bar
		DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
				this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
		drawer.setDrawerListener(toggle);
		toggle.syncState();

		NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
		navigationView.setNavigationItemSelectedListener(this);


		//
		// Test
		//
		text = (TextView) findViewById(R.id.test);
	}

	/**
	 * Called when the back button is pressed
	 */
	@Override
	public void onBackPressed() {
		DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		if (drawer.isDrawerOpen(GravityCompat.START)) {
			drawer.closeDrawer(GravityCompat.START);
		} else {
			super.onBackPressed();
		}
	}

	/**
	 * Called xhen the main menu is created, inflated the menu
	 * @param menu the menu
	 * @return true
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////// Menus //////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * The response of the main menu
	 * @param item the selected item
	 * @return
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	/**
	 * The response of the sidebar menu
	 * @param item the selected item
	 * @return true
	 */
	@SuppressWarnings("StatementWithEmptyBody")
	@Override
	public boolean onNavigationItemSelected(MenuItem item) {
		// Handle navigation view item clicks here.
		int id = item.getItemId();

		// New
		if (id == R.id.new_file) {

		}

		// Open
		else if (id == R.id.open_file) {
			FileChooserDialog dialog = new FileChooserDialog(this);
			dialog.setShowCancelButton(true);
			dialog.setFilter(".*csu");
			dialog.show();
			dialog.addListener(new FileChooserDialog.OnFileSelectedListener() {
				public void onFileSelected(Dialog source, File file) {
					source.hide();
					try {
						setGame(new Game(file));
						setFile(file);
					} catch (XmlError e) {
						Toast toast = Toast.makeText(source.getContext(),
								I18n.tr(R.string.main_activity_open_error,file.getName()),
								Toast.LENGTH_LONG);
						toast.show();
					}
				}

				public void onFileSelected(Dialog source, File folder, String name) {
				}
			});
		}

		// Save
		else if (id == R.id.save_file) {
			
		}

		// About
		else if (id == R.id.about) {
			Intent intent = new Intent(MainActivity.this, AboutActivity.class);
			startActivity(intent);
		}

		DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		drawer.closeDrawer(GravityCompat.START);
		return true;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////// Functions //////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////// Setters and getters ////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Return the game
	 * @return the game
	 */
	public Game getGame(){
		return game_;
	}

	/**
	 * Return the file
	 * @return the file
	 */
	public File getFile(){
		return file_;
	}

	/**
	 * Set the game
	 * @param game the game
	 */
	public void setGame(Game game){
		game_ = game;
		text.setText(game_.toString());
	}

	/**
	 * Set the file
	 * @param file the file
	 */
	public void setFile(File file){
		file_ = file;
	}
}
