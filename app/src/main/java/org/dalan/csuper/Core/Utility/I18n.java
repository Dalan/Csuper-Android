/*
* I18n.java
*
* Copyright 2014-2016
* Remi BERTHO <remi.bertho@openmailbox.org>
*
* This file is part of Csuper.
*
* Csuper is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* Csuper is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
*
*
*/

package org.dalan.csuper.Core.Utility;

import android.content.Context;

/**
 * The Internationalisation class
 * @author Rémi BERTHO
 */
public class I18n {
	private static Context context;

	/**
	 * Set the context
	 * @param c the context
	 */
	public static void setContext(Context c){
		context = c;
	}

	/**
	 * Return the context saved
	 * @return the context
	 */
	public static Context getContext(){
		return context;
	}

	/**
	 * Retreive a string from his id
	 * @param id the id of the string
	 * @return the string
	 */
	public static String tr(int id){
		return context.getResources().getString(id);
	}

	/**
	 * Retreive a string from his id
	 * @param id the id of the string
	 * @param o1 the first parameter
	 * @return the string
	 */
	public static String tr(int id, Object o1){
		return context.getResources().getString(id,o1);
	}

	/**
	 * Retreive a string from his id
	 * @param id the id of the string
	 * @param o1 the first parameter
	 * @param o2 the second parameter
	 * @return the string
	 */
	public static String tr(int id, Object o1, Object o2){
		return context.getResources().getString(id,o1,o2);
	}

	/**
	 * Retreive a string from his id
	 * @param id the id of the string
	 * @param o1 the first parameter
	 * @param o2 the second parameter
	 * @param o3 the third parameter
	 * @return the string
	 */
	public static String tr(int id, Object o1, Object o2, Object o3){
		return context.getResources().getString(id,o1,o2,o3);
	}

	/**
	 * Retreive a string from his id
	 * @param id the id of the string
	 * @param o1 the first parameter
	 * @param o2 the second parameter
	 * @param o3 the third parameter
	 * @param o4 the fourth parameter
	 * @return the string
	 */
	public static String tr(int id, Object o1, Object o2, Object o3, Object o4){
		return context.getResources().getString(id,o1,o2,o3,o4);
	}

	/**
	 * Retreive a string from his id
	 * @param id the id of the string
	 * @param o1 the first parameter
	 * @param o2 the second parameter
	 * @param o3 the third parameter
	 * @param o4 the fourth parameter
	 * @param o5 the fifth parameter
	 * @return the string
	 */
	public static String tr(int id, Object o1, Object o2, Object o3, Object o4, Object o5){
		return context.getResources().getString(id,o1,o2,o3,o4,o5);
	}
}
