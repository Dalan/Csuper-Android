/*
* GameConfiguration.java
*
* Copyright 2014-2016
* Remi BERTHO <remi.bertho@openmailbox.org>
*
* This file is part of Csuper.
*
* Csuper is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* Csuper is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
*
*
*/

package org.dalan.csuper.Core;

import android.os.Parcel;
import android.os.Parcelable;

import org.dalan.csuper.Core.Listener.GameConfigurationListener;
import org.dalan.csuper.Core.Utility.I18n;
import org.dalan.csuper.Core.Utility.Share;
import org.dalan.csuper.R;
import org.jdom2.Content;
import org.jdom2.Element;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represent a game configuration
 * @author Rémi BERTHO
 */
public class GameConfiguration implements Cloneable, Parcelable {
	///////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////// Attributes /////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////


	/**
	 * Number maximum or minimum that can reach a player
	 */
	private double nb_max_min_ = 0;

	/**
	 * True if the first is those has the maximum of points, false otherwise
	 */
	private boolean use_maximum_ = true;

	/**
	 * True if this is a turn-based game, false otherwise
	 */
	private boolean turn_based_ = false;

	/**
	 * True if the game use a distributor, false otherwise
	 */
	private boolean use_distributor_ = false;

	/**
	 * The number of decimal place which are display
	 */
	private int decimal_place_ = 0;

	/**
	 * True if the game use a maximum, false if it's a minimum
	 */
	private boolean max_winner_ = true;

	/**
	 * The name of the game configuration
	 */
	private String name_ = "";

	/**
	 * The score of all players in the beginning of the game
	 */
	private double initial_score_ = 0;

	/**
	 * The number of turn before the distributor change
	 */
	private int nb_turn_distributor_ = 1;

	/**
	 * This is the list of listeners
	 */
	private List<GameConfigurationListener> listeners_;


	/**
	 * The version
	 */
	private final static double version_ = 1.1;


	///////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////// Constructor ////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////


	/**
	 * Default constructor
	 */
	public GameConfiguration(){
		listeners_ = new ArrayList<>();
	}

	/**
	 * Constructor with all parameters
	 * @param nb_max_min nb max min
	 * @param use_maximum use maximum
	 * @param turn_based turn based
	 * @param use_distributor use distributor
	 * @param decimal_place decimal place
	 * @param max_winner max winner
	 * @param name name
	 * @param initial_score initial score
	 * @param nb_turn_distributor nb turn distributor
	 */
	public GameConfiguration(double nb_max_min,
	                          boolean use_maximum,
	                          boolean turn_based,
	                          boolean use_distributor,
	                          int decimal_place,
	                          boolean max_winner,
	                          String name,
	                          double initial_score,
	                          int nb_turn_distributor){
		this();
		nb_max_min_ =  nb_max_min;
		use_maximum_ = use_maximum;
		turn_based_ = turn_based;
		use_distributor_ = use_distributor;
		decimal_place_ = decimal_place;
		max_winner_ = max_winner;
		name_ = name;
		initial_score_ = initial_score;
		nb_turn_distributor_ = nb_turn_distributor;
	}

	/**
	 * Constructor from an XML element
	 * @param e the xml element
	 * @param version the version of the xml element
	 */
	public GameConfiguration(Element e, double version){
		this();

		List<Element> childs = e.getChildren();

		Element tmp_element = childs.get(0);
		Content tmp_content = tmp_element.getContent(0);
		nb_max_min_ = Share.stringToDouble(tmp_content.getValue());

		tmp_element = childs.get(1);
		tmp_content = tmp_element.getContent(0);
		use_maximum_ = Share.stringToBool(tmp_content.getValue());

		tmp_element = childs.get(2);
		tmp_content = tmp_element.getContent(0);
		turn_based_ = Share.stringToBool(tmp_content.getValue());

		tmp_element = childs.get(3);
		tmp_content = tmp_element.getContent(0);
		use_distributor_ = Share.stringToBool(tmp_content.getValue());

		tmp_element = childs.get(4);
		tmp_content = tmp_element.getContent(0);
		decimal_place_ = Share.stringToInt(tmp_content.getValue());

		tmp_element = childs.get(5);
		tmp_content = tmp_element.getContent(0);
		max_winner_ = Share.stringToBool(tmp_content.getValue());

		tmp_element = childs.get(6);
		tmp_content = tmp_element.getContent(0);
		name_ = tmp_content.getValue();

		tmp_element = childs.get(7);
		tmp_content = tmp_element.getContent(0);
		initial_score_ = Share.stringToDouble(tmp_content.getValue());

		tmp_element = childs.get(8);
		tmp_content = tmp_element.getContent(0);
		nb_turn_distributor_ = Share.stringToInt(tmp_content.getValue());
	}

	/**
	 * Clone the game configuration
	 * @return the clone
	 */
	@Override
	public GameConfiguration clone(){
		GameConfiguration cl = null;

		try {
			cl = (GameConfiguration)super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}

		cl.nb_max_min_ = nb_max_min_;
		cl.use_maximum_ = use_maximum_;
		cl.turn_based_ = turn_based_;
		cl.use_distributor_ = use_distributor_;
		cl.decimal_place_ = decimal_place_;
		cl.max_winner_ = max_winner_;
		cl.name_ = name_;
		cl.initial_score_ = initial_score_;
		cl.nb_turn_distributor_ = nb_turn_distributor_;

		return cl;
	}


	///////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////// Function ///////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////


	/**
	 * Check if two game configuration are equals
	 * @param gc the gale configuration
	 * @return true if they are equals, false otherwise
	 */
	public boolean equals(GameConfiguration gc){
		return ((nb_max_min_ == gc.nb_max_min_) &&
				(use_maximum_ == gc.use_maximum_) &&
				(turn_based_ == gc.turn_based_) &&
				(use_distributor_ == gc.use_distributor_) &&
				(decimal_place_ == gc.decimal_place_) &&
				(max_winner_ == gc.max_winner_) &&
				(name_.equals(gc.name_)) &&
				(initial_score_ == gc.initial_score_) &&
				(nb_turn_distributor_ == gc.nb_turn_distributor_));
	}

	/**
	 * Convert the game configuration in a string but without the name
	 * @return the string
	 */
	public String toStringWithoutName(){
		StringBuilder sb = new StringBuilder();

		sb.append(I18n.tr(R.string.game_configuration_use_maximum)).append(" ").append(Share.boolToStringTr(use_maximum_)).append("\n");
		sb.append(I18n.tr(R.string.game_configuration_nb_max_min)).append(" ").append(Share.doubleToString(nb_max_min_, decimal_place_)).append("\n");
		sb.append(I18n.tr(R.string.game_configuration_initial_score)).append(" ").append(Share.doubleToString(initial_score_, decimal_place_)).append("\n");
		sb.append(I18n.tr(R.string.game_configuration_decimal_place)).append(" ").append(Share.intToString(decimal_place_)).append("\n");
		sb.append(I18n.tr(R.string.game_configuration_max_winner)).append(" ").append(Share.boolToStringTr(max_winner_)).append("\n");
		sb.append(I18n.tr(R.string.game_configuration_turn_based)).append(" ").append(Share.boolToStringTr(turn_based_)).append("\n");
		sb.append(I18n.tr(R.string.game_configuration_use_ditributor)).append(" ").append(Share.boolToStringTr(use_distributor_)).append("\n");
		sb.append(I18n.tr(R.string.game_configuration_nb_turn_distributor)).append(" ").append(Share.intToString(nb_turn_distributor_));

		return sb.toString();
	}

	/**
	 * Convert the game configuration in a string
	 * @return the string
	 */
	@Override
	public  String toString(){
		StringBuilder sb = new StringBuilder();

		sb.append(I18n.tr(R.string.game_configuration_name)).append(" ").append(name_).append("\n");
		sb.append(toStringWithoutName());

		return sb.toString();
	}

	/**
	 * Create an XML element based on the game configuration
	 * @return the XML element
	 */
	public Element toXmlElement(){
		Element e = new Element("game_configuration");

		e.addContent(new Element("nb_max_min").setText(Share.doubleToString(nb_max_min_)));
		e.addContent(new Element("max_winner").setText(Share.boolToString(max_winner_)));
		e.addContent(new Element("turn_by_turn").setText(Share.boolToString(turn_based_)));
		e.addContent(new Element("use_distributor").setText(Share.boolToString(use_distributor_)));
		e.addContent(new Element("decimal_place").setText(Share.intToString(decimal_place_)));
		e.addContent(new Element("use_maximum").setText(Share.boolToString(use_maximum_)));
		e.addContent(new Element("name").setText(name_));
		e.addContent(new Element("begin_score").setText(Share.doubleToString(initial_score_)));
		e.addContent(new Element("nb_turn_distributor").setText(Share.intToString(nb_turn_distributor_)));

		return e;
	}


	///////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////// Listener ///////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////


	/**
	 * Add a listener to the game configuration
	 * @param l the listener
	 */
	public void addListener(GameConfigurationListener l){
		listeners_.add(l);
	}

	/**
	 * Remove a listener to the game configuration
	 * @param l the listener
	 */
	public void removeListener(GameConfigurationListener l){
		listeners_.remove(l);
	}

	/**
	 * Call onGameChange to all listeners
	 */
	public void sendSignalChange(){
		for (GameConfigurationListener gcl : listeners_)
			gcl.onGameConfigurationChange();
	}


	///////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////// Getter and setter /////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////


	public double getNbMaxMin() {
		return nb_max_min_;
	}

	public void setNbMaxMin(double nb_max_min_) {
		this.nb_max_min_ = nb_max_min_;
		sendSignalChange();
	}

	public boolean getUseMaximum() {
		return use_maximum_;
	}

	public void setUseMaximum(boolean use_maximum_) {
		this.use_maximum_ = use_maximum_;
		sendSignalChange();
	}

	public boolean getTurnBased() {
		return turn_based_;
	}

	public void setTurnBased(boolean turn_based_) {
		this.turn_based_ = turn_based_;
		sendSignalChange();
	}

	public boolean getUseDistributor() {
		return use_distributor_;
	}

	public void setUseDistributor(boolean use_distributor_) {
		this.use_distributor_ = use_distributor_;
		sendSignalChange();
	}

	public int getDecimalPlace() {
		return decimal_place_;
	}

	public void setDecimalPlace(int decimal_place_) {
		this.decimal_place_ = decimal_place_;
		sendSignalChange();
	}

	public boolean getMaxWinner() {
		return max_winner_;
	}

	public void setMaxWinner(boolean max_winner_) {
		this.max_winner_ = max_winner_;
		sendSignalChange();
	}

	public String getName() {
		return name_;
	}

	public void setName(String name_) {
		this.name_ = name_;
		sendSignalChange();
	}

	public double getInitialScore() {
		return initial_score_;
	}

	public void setInitialScore(double initial_score_) {
		this.initial_score_ = initial_score_;
		sendSignalChange();
	}

	public int getNbTurnDistributor() {
		return nb_turn_distributor_;
	}

	public void setNbTurnDistributor(int nb_turn_distributor_) {
		this.nb_turn_distributor_ = nb_turn_distributor_;
		sendSignalChange();
	}

	public static double getVersion() {
		return version_;
	}


	///////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////// Parcelable /////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeDouble(nb_max_min_);
		dest.writeInt(max_winner_ ? 1 : 0);
		dest.writeInt(turn_based_ ? 1 : 0);
		dest.writeInt(use_distributor_ ? 1 : 0);
		dest.writeInt(decimal_place_);
		dest.writeInt(use_maximum_ ? 1 : 0);
		dest.writeString(name_);
		dest.writeDouble(initial_score_);
		dest.writeInt(nb_turn_distributor_);
	}

	public static final Parcelable.Creator<GameConfiguration> CREATOR = new Parcelable.Creator<GameConfiguration>(){

		@Override
		public GameConfiguration createFromParcel(Parcel source) {
			return new GameConfiguration(source);
		}

		@Override
		public GameConfiguration[] newArray(int size) {
			return new GameConfiguration[size];
		}
	};

	public GameConfiguration(Parcel in) {
		this();
		nb_max_min_ = in.readDouble();
		max_winner_ = in.readInt() != 0;
		turn_based_ = in.readInt() != 0;
		use_distributor_ = in.readInt() != 0;
		decimal_place_ = in.readInt();
		use_maximum_ = in.readInt() != 0;
		name_ = in.readString();
		initial_score_ = in.readDouble();
		nb_turn_distributor_ = in.readInt();
	}
}
