/*
* Game.java
*
* Copyright 2014-2016
* Remi BERTHO <remi.bertho@openmailbox.org>
*
* This file is part of Csuper.
*
* Csuper is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* Csuper is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
*
*
*/

package org.dalan.csuper.Core;

import org.dalan.csuper.Core.Exception.FileError;
import org.dalan.csuper.Core.Exception.NotFound;
import org.dalan.csuper.Core.Exception.OutOfRange;
import org.dalan.csuper.Core.Exception.WrongUse;
import org.dalan.csuper.Core.Exception.XmlError;
import org.dalan.csuper.Core.Listener.GameConfigurationListener;
import org.dalan.csuper.Core.Listener.GameListener;
import org.dalan.csuper.Core.Utility.I18n;
import org.dalan.csuper.Core.Utility.Share;
import org.dalan.csuper.Core.Utility.Wrapper;
import org.dalan.csuper.R;
import org.jdom2.Content;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * This class represent a game
 */
public class Game implements Cloneable, GameConfigurationListener, GameListener {
	///////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////// Attributes /////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * The number of player
	 */
	private int nb_player_;

	/**
	 * The game configuration
	 */
	private GameConfiguration config_;

	/**
	 * The players
	 */
	private List<Player> players_;

	/**
	 * The distributor
	 */
	private int distributor_ = 0;

	/**
	 * The number of turn already played by the distributor distributor
	 */
	private int nb_turn_distributor_ = 0;

	/**
	 * The date of creation
	 */
	private Calendar date_;

	/**
	 * The maximal size of a player name
	 * Only for compatibility issue
	 */
	private int size_max_name_ = 100;

	/**
	 * This is the list of listeners
	 */
	private List<GameListener> listeners_;


	/**
	 * The version
	 */
	private static final double version_ = 1.5;

	///////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////// Constructor ////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Default constructor
	 */
	private Game(){
		players_ = new ArrayList<>();
		date_ = new GregorianCalendar();
		listeners_ = new ArrayList<>();
		listeners_.add(this);
	}

	/**
	 * Constructor
	 * @param nb_player the number of player
	 * @param game_config the game configuration used
	 */
	public Game(int nb_player,GameConfiguration game_config){
		this();
		config_ = game_config.clone();
		nb_player_ = nb_player;

		for (int i = 0 ; i < nb_player ; i++){
			players_.add(new Player(config_));
		}

		connectSignals();
	}

	/**
	 * Constructor from a file
	 * @param file the file
	 */
	public Game(File file) throws XmlError{
		this();

		SAXBuilder b = new SAXBuilder();
		Document doc;
		try {
			doc = b.build(file);
		} catch (Exception e) {
			XmlError error = new XmlError("Cannot open the file " + file.getAbsolutePath());
			error.initCause(e);
			throw error;
		}

		Element root = doc.getRootElement();
		if (! root.getName().equals("csu"))
			throw new XmlError("This file is not a CSU file, it's a " + root.getName() + " file.");

		List<Element> childs = root.getChildren();
		int child_pos = 0;

		// Version
		Element tmp_element = childs.get(child_pos);
		child_pos++;
		Content tmp_content = tmp_element.getContent(0);
		double file_version = Share.stringToDouble(tmp_content.getValue());
		if (file_version > version_)
			throw new XmlError("This version of Csuper only support game file version of " + version_);
		double game_config_version;
		if (file_version <= 1.4)
			game_config_version = 1.0;
		else
			game_config_version = 1.1;

		//Size max of a name
		tmp_element = childs.get(child_pos);
		child_pos++;
		tmp_content = tmp_element.getContent(0);
		size_max_name_ = Share.stringToInt(tmp_content.getValue());


		// Date
		tmp_element = childs.get(child_pos);
		child_pos++;
		List<Element> date = tmp_element.getChildren();

		tmp_element = date.get(0);
		tmp_content = tmp_element.getContent(0);
		int year = Share.stringToInt(tmp_content.getValue());

		tmp_element = date.get(1);
		tmp_content = tmp_element.getContent(0);
		int month = Share.stringToInt(tmp_content.getValue()) - 1;

		tmp_element = date.get(2);
		tmp_content = tmp_element.getContent(0);
		int day = Share.stringToInt(tmp_content.getValue());

		date_.set(year,month,day);


		// Nb player
		tmp_element = childs.get(child_pos);
		child_pos++;
		tmp_content = tmp_element.getContent(0);
		nb_player_ = Share.stringToInt(tmp_content.getValue());

		// Distributor
		tmp_element = childs.get(child_pos);
		child_pos++;
		tmp_content = tmp_element.getContent(0);
		distributor_ = Share.stringToInt(tmp_content.getValue());

		if (file_version >= 1.5){
			tmp_element = childs.get(child_pos);
			child_pos++;
			tmp_content = tmp_element.getContent(0);
			nb_turn_distributor_ = Share.stringToInt(tmp_content.getValue());
		}

		// Game configuration
		tmp_element = childs.get(child_pos);
		child_pos++;
		config_ = new GameConfiguration(tmp_element,game_config_version);


		//Players
		for (int i=0 ; i<nb_player_ ; i++){
			tmp_element = childs.get(child_pos);
			child_pos++;
			players_.add(new Player(tmp_element));
		}

		connectSignals();
	}



	/**
	 * Constructor from a filename
	 * @param filename the filename
	 */
	public Game(String filename) throws XmlError{
		this(new File(filename));
	}

	/**
	 * Clone the game
	 * @return the clone
	 */
	@Override
	public Game clone(){
		Game game;

		try {
			game = (Game)super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}

		game.addListener(game);
		game.date_ = (Calendar)date_.clone();
		game.nb_player_ = nb_player_;
		game.distributor_ = distributor_;
		game.nb_turn_distributor_ = nb_turn_distributor_;
		game.config_ = config_.clone();

		game.players_ = new ArrayList<>();
		for (Player p : players_)
			game.players_.add(p.clone());

		game.connectSignals();

		return game;
	}


	///////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////// Function ///////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Write the game to a file
	 * @param filename the filename
	 */
	public void writeToFile(String filename) throws FileError{
		Document doc = new Document();
		Element root = new Element("csu");
		doc.setRootElement(root);

		// Version + size max name
		root.addContent(new Element("version").setText(Share.doubleToString(version_)));
		root.addContent(new Element("size_max_name").setText(Share.intToString(size_max_name_)));

		//Date
		Element date = new Element("date");
		date.addContent(new Element("year").setText(Share.intToString(date_.get(Calendar.YEAR))));
		date.addContent(new Element("month").setText(Share.intToString(date_.get(Calendar.MONTH)+1)));
		date.addContent(new Element("day").setText(Share.intToString(date_.get(Calendar.DAY_OF_MONTH))));
		root.addContent(date);

		// Nb player + distributor
		root.addContent(new Element("nb_player").setText(Share.intToString(nb_player_)));
		root.addContent(new Element("distributor").setText(Share.intToString(distributor_)));
		root.addContent(new Element("nb_turn_distributor").setText(Share.intToString(nb_turn_distributor_)));

		//Game configuration
		root.addContent(config_.toXmlElement());

		//Players
		for (Player p :players_)
			root.addContent(p.toXmlElement());

		// Write the file
		XMLOutputter xmlOutput = new XMLOutputter();
		xmlOutput.setFormat(Format.getPrettyFormat());
		try {
			xmlOutput.output(doc, new FileWriter(filename));
		} catch (IOException e) {
			FileError error = new FileError("Error while writing the CSU file " + filename);
			error.initCause(e);
			throw error;
		}
	}

	/**
	 * Write the game to a file
	 * @param file the file
	 */
	public void writeToFile(File file) throws FileError{
		writeToFile(file.getAbsoluteFile());
	}

	/**
	 * Calculate the maximal number of turn
	 * @return the maximal number of turn
	 */
	public int getMaxNbTurn() {
		int max_turn = 0;

		for (Player p : players_)
			max_turn = Math.max(max_turn, p.getNbTurn());

		return max_turn;
	}



	///////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////// Listener ///////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Add a listener to the game configuration
	 * @param l the listener
	 */
	public void addListener(GameListener l){
		listeners_.add(l);
	}

	/**
	 * Remove a listener to the game configuration
	 * @param l the listener
	 */
	public void removeListener(GameListener l){
		listeners_.remove(l);
	}

	/**
	 * Call onGameChange to all listeners
	 */
	public void sendSignalChange(){
		for (GameListener gl : listeners_)
			gl.onGameChange();
	}


	/**
	 * Call onGamePointsChange to all listeners
	 */
	public void sendSignalPointsChange(){
		for (GameListener gl : listeners_)
			gl.onGamePointsChange();
	}

	/**
	 * Call onGameDistributorChange to all listeners
	 */
	public void sendSignalDistributorChange(){
		for (GameListener gl : listeners_)
			gl.onGameDistributorChange();
	}

	/**
	 * Call onGameExceedMaxNumber to all listeners
	 */
	public void sendSignalExceedMaxNumber(){
		for (GameListener gl : listeners_)
			gl.onGameExceedMaxNumber();
	}

	/**
	 * Connect all the signal for internal use
	 */
	void connectSignals(){
		config_.addListener(this);
	}

	/**
	 * Change the initial score of all player when the game configuration change
	 */
	@Override
	public void onGameConfigurationChange() {
		List<Double> points = new ArrayList<>();
		for (int i = 0 ; i<nb_player_ ; i++)
			points.add(config_.getInitialScore());
		//editTurn(0,points)
	}

	/**
	 * Do nothing
	 */
	@Override
	public void onGameChange() {

	}

	/**
	 * Calculate the new ranking when the points change
	 */
	@Override
	public void onGamePointsChange() {
		//rankingCalculation();
	}

	/**
	 * Do nothing
	 */
	@Override
	public void onGameDistributorChange() {

	}

	/**
	 * Do nothing
	 */
	@Override
	public void onGameExceedMaxNumber() {

	}


	///////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////// Getter /////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Get the index of a player from his name
	 * @param player_name the player name
	 * @return the index
	 * @throws NotFound if the player name doesn't exist
	 */
	private int getPlayerIndex(String player_name) throws NotFound{
		for (int i=0 ; i<nb_player_ ; i++)
		{
			if (player_name.equals(players_.get(i).getName()))
				return i;
		}
		throw new NotFound("Player " + player_name + " is not found in the game");
	}

	/**
	 * Return the player
	 * @param index the index of the player
	 * @return the player
	 * @throws OutOfRange if index is greater the the number of player
	 */
	protected Player getPlayer(int index) throws OutOfRange{
		if (index >= nb_player_)
			throw new OutOfRange("Cannot access to the " + index + "th player, there is only " + nb_player_ + " player");
		return players_.get(index);
	}

	/**
	 * Return the player
	 * @param player_name the player name
	 * @return the player
	 * @throws OutOfRange if index is greater the the number of player
	 */
	protected Player getPlayer(String player_name) throws OutOfRange{
		return getPlayer(getPlayerIndex(player_name));
	}

	/**
	 * Return the version
	 * @return the version
	 */
	public double getVersion(){
		return version_;
	}

	/**
	 * Return the size max of a name
	 * @return the size_max_name_
	 */
	public int getSizeMaxName(){
		return size_max_name_;
	}

	/**
	 * Return the date
	 * @return the date
	 */
	public Calendar getDate(){
		return date_;
	}

	/**
	 * Return the nb player
	 * @return the nb player
	 */
	public int getNbPlayer(){
		return nb_player_;
	}

	/**
	 * Return the game configuration
	 * @return the game configuration
	 */
	public GameConfiguration getConfig(){
		return config_;
	}

	/**
	 * Return the distributor
	 * @return the distributor
	 */
	public int getDistributor(){
		return distributor_;
	}

	/**
	 * Return nb turn distributor
	 * @return the nb turn distributor
	 */
	public int getNbTurnDistributor(){
		return nb_turn_distributor_;
	}

	/**
	 * Return the player name
	 * @param player_index the player index
	 * @return the player name
	 * @throws OutOfRange if index is greater the the number of player
	 */
	public String getPlayerName(int player_index) throws OutOfRange{
		return getPlayer(player_index).getName();
	}

	/**
	 * Return the distributor name
	 * @return the distributor name
	 */
	public String getDistributorName(){
		return players_.get(distributor_).getName();
	}

	/**
	 * Return the number of turn of a player
	 * @param player_index the player index
	 * @return the number of turn
	 * @throws OutOfRange if index is greater than the number of player
	 */
	public int getNbTurn(int player_index) throws OutOfRange{
		return getPlayer(player_index).getNbTurn();
	}

	/**
	 * Return the number of turn of a player
	 * @param player_name the player name
	 * @return the number of turn
	 * @throws NotFound if the player name doesn't exist
	 */
	public int getNbTurn(String player_name) throws NotFound{
		return getPlayer(player_name).getNbTurn();
	}

	/**
	 * Return the points of a player in a given turn
	 * @param player_index the player index
	 * @param turn the turn,
	 * @return the points
	 * @throws OutOfRange if index is greater than the number of player
	 * @throws OutOfRange if turn is greater than the number of turn
	 */
	public double getPoints(int player_index, int turn) throws OutOfRange{
		return getPlayer(player_index).getPoints(turn);
	}

	/**
	 * Return the points of a player in a given turn
	 * @param player_name the player name
	 * @param turn the turn
	 * @return the points
	 * @throws OutOfRange if turn is greater than the number of turn
	 * @throws NotFound if the player name doesn't exist
	 */
	public double getPoints(String player_name, int turn) throws OutOfRange, NotFound{
		return getPlayer(player_name).getPoints(turn);
	}

	/**
	 * Return the total points of a player in a given turn
	 * @param player_index the player index
	 * @param turn the turn,
	 * @return the total points
	 * @throws OutOfRange if index is greater than the number of player
	 * @throws OutOfRange if turn is greater than the number of turn
	 */
	public double getTotalPoints(int player_index, int turn) throws OutOfRange{
		return getPlayer(player_index).getTotalPoints(turn);
	}

	/**
	 * Return the total points of a player in the last turn
	 * @param player_index the player index
	 * @return the total points
	 * @throws OutOfRange if index is greater than the number of player
	 */
	public double getTotalPoints(int player_index) throws OutOfRange{
		return getPlayer(player_index).getTotalPoints();
	}

	/**
	 * Return the total points of a player in a given turn
	 * @param player_name the player name
	 * @param turn the turn
	 * @return the total points
	 * @throws OutOfRange if turn is greater than the number of turn
	 * @throws NotFound if the player name doesn't exist
	 */
	public double getTotalPoints(String player_name, int turn) throws OutOfRange, NotFound{
		return getPlayer(player_name).getTotalPoints(turn);
	}

	/**
	 * Return the total points of a player in the last turn
	 * @param player_name the player name
	 * @return the total points
	 * @throws NotFound if the player name doesn't exist
	 */
	public double getTotalPoints(String player_name) throws NotFound{
		return getPlayer(player_name).getTotalPoints();
	}

	/**
	 * Return the ranking of a player
	 * @param player_index the player index
	 * @return the ranking
	 * @throws OutOfRange if index is greater than the number of player
	 */
	public int getRanking(int player_index) throws OutOfRange{
		return getPlayer(player_index).getRanking();
	}

	/**
	 * Return the ranking of a player
	 * @param player_index the player index
	 * @param turn the turn
	 * @return the ranking
	 * @throws OutOfRange if turn is greater than the number of turn
	 * @throws OutOfRange if index is greater than the number of player
	 * @throws WrongUse if this function is used in a non turn game with a specific turn
	 */
	public int getRanking(int player_index, int turn) throws OutOfRange,WrongUse{
		if (!(getConfig().getTurnBased())){
			throw new WrongUse("The ranking function should only be used in a turn based game when" +
					" a specific turn is specify");
		}

		if (!(getPlayer(player_index).hasTurn(turn))){
			throw new OutOfRange("Cannot access to the " + turn + "th turn, there is only " +
					getPlayer(player_index).getNbTurn() + " turn");
		}

		List<Double> sort_points = new ArrayList<>();

		for (Player player : players_)
			sort_points.add(player.getTotalPoints(turn));

		// Sort the points base on the first way
		if (getConfig().getMaxWinner())
			Collections.sort(sort_points,Collections.reverseOrder());
		else
			Collections.sort(sort_points);

		double points_player = getTotalPoints(player_index,turn);
		for (int i=0 ; i<nb_player_ ; i++){
			if (sort_points.get(i).equals(points_player))
				return i+1;
		}

		return 0;
	}

	/**
	 * Return the ranking of a player
	 * @param player_name the player name
	 * @return the ranking
	 * @throws NotFound if the player name doesn't exist
	 */
	public int getRanking(String player_name) throws NotFound{
		return getPlayer(player_name).getRanking();
	}

	/**
	 * Return the ranking of a player
	 * @param player_name the player name
	 * @param turn the turn
	 * @return the ranking
	 * @throws NotFound if the player name doesn't exist
	 * @throws OutOfRange if turn is greater than the number of turn
	 * @throws WrongUse if this function is used in a non turn game with a specific turn
	 */
	public int getRanking(String player_name, int turn) throws NotFound, OutOfRange, WrongUse{
		return getRanking(getPlayerIndex(player_name),turn);
	}

	/**
	 * Return true if the player has points on the turn, false otherwise
	 * @param player_index the player index
	 * @param turn the turn
	 * @return true if the player has points on the turn, false otherwise
	 * @throws OutOfRange if index is greater than the number of player
	 */
	boolean hasTurn(int player_index, int turn) throws OutOfRange{
		return getPlayer(player_index).hasTurn(turn);
	}

	/**
	 * Return true if the player has points on the turn, false otherwise
	 * @param player_name the player name
	 * @param turn turn the turn
	 * @return true if the player has points on the turn, false otherwise
	 * @throws NotFound if the player name doesn't exist
	 */
	boolean hasTurn(String player_name, int turn) throws NotFound{
		return getPlayer(player_name).hasTurn(turn);
	}


	///////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////// Setter /////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Set the size maximum of a name
	 * @param size_max_name the size_max_name
	 */
	public void setSizeMaxName(int size_max_name) {
		size_max_name_ = size_max_name;
		sendSignalChange();
	}

	/**
	 * Set the date
	 * @param date the date
	 */
	public void setDate(Calendar date) {
		date_ = date;
		sendSignalChange();
	}

	/**
	 * Set the number of player
	 * @param nb_player the nb_player
	 */
	public void setNbPlayer(int nb_player) {
		nb_player_ = nb_player;
		sendSignalChange();
	}

	/**
	 * Set the distributor
	 * @param distributor the distributor
	 * @throws OutOfRange if index is greater the than number of player
	 */
	public void setDistributor(int distributor) throws OutOfRange{
		if (distributor > nb_player_){
			throw new OutOfRange("Cannot set the " + distributor+1 + "th player distributor, there " +
					"is only " + nb_player_ + " player");
		}
		distributor_ = distributor;
		sendSignalChange();
		sendSignalDistributorChange();
	}

	/**
	 * Set the distributor
	 * @param distributor_name the distributor name
	 * @throws NotFound if the player name doesn't exist
	 */
	public void setDistributor(String distributor_name) throws NotFound{
		setDistributor(getPlayerIndex(distributor_name));
	}

	/**
	 * Set a player name
	 * @param index the index of the player
	 * @param name the name
	 * @throws OutOfRange if index is greater the than number of player
	 */
	public void setPlayerName(int index, String name) throws OutOfRange{
		if (index > nb_player_){
			throw new OutOfRange("Cannot set the " + index+1 + "th player distributor, there " +
					"is only " + nb_player_ + " player");
		}
		players_.get(index).setName(name);
		sendSignalChange();
	}

	/**
	 * Set the nb turn distributor
	 * @param nb_turn_distributor the nb_turn_distributor
	 */
	public void setNbTurnDistributor(int nb_turn_distributor) {
		nb_turn_distributor_ = nb_turn_distributor;
		sendSignalChange();
	}


	///////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////// ToString ///////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Convert the game in a string
	 * @return the string
	 */
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		int line_size = 1;
		Wrapper<Integer> wrapped_line_size = new Wrapper<>(line_size);

		sb.append(toStringHeader()).append("\n");
		sb.append(config_.toString()).append("\n");
		sb.append(toStringDistributor()).append("\n");
		sb.append(toStringNames(wrapped_line_size, true)).append("\n");
		line_size = wrapped_line_size.get();
		sb.append(toStringLine(line_size)).append("\n");
		sb.append(toStringAllPoints());
		sb.append(toStringLine(line_size)).append("\n");
		sb.append(toStringTotalPoints()).append("\n");
		sb.append(toStringLine(line_size)).append("\n");
		sb.append(toStringRanking());

		return  sb.toString();
	}

	/**
	 * Convert the header to a string
	 * @return the string
	 */
	private String toStringHeader(){
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		return I18n.tr(R.string.game_header,
				format.format(date_.getTime()),
				version_,
				size_max_name_,
				nb_player_,
				getMaxNbTurn());
	}

	/**
	 * Convert the distributor to a string
	 * @return the string
	 */
	private String toStringDistributor() {
		if (config_.getUseDistributor())
			return I18n.tr(R.string.game_distributor,getDistributorName(),nb_turn_distributor_);
		return "";
	}

	/**
	 * Convert the names to a string and calculate the line size
	 * @param wrapped_line_size the size of the line
	 * @param change_line_size indicate if the function must change the line size
	 * @return the string
	 */
	private String toStringNames(Wrapper<Integer> wrapped_line_size, boolean change_line_size) {
		StringBuilder sb = new StringBuilder();
		int i;
		int line_size = wrapped_line_size.get();

		sb.append(I18n.tr(R.string.game_name)).append(" ");

		for (Player p : players_)
		{
			String name = p.getName();
			sb.append(name);

			for (i=name.length() ; i < 4 ; i++)
			{
				sb.append(" ");
				if (change_line_size)
					line_size+=1;
			}

			sb.append(" | ");

			if (change_line_size)
				line_size += name.length() + 3;
		}

		wrapped_line_size.set(line_size);
		return sb.toString();
	}

	/**
	 * Create a line to a string
	 * @param line_size the size of the line
	 * @return the string
	 */
	private String toStringLine(int line_size) {
		StringBuilder sb = new StringBuilder();
		sb.append("\t");

		for (int i=0 ; i<line_size ; i++)
			sb.append("-");

		return sb.toString();
	}

	/**
	 * Convert all the points to a string
	 * @return the string
	 */
	private String toStringAllPoints() {
		StringBuilder sb = new StringBuilder();
		int i,j;

		for (i=0 ; i<=getMaxNbTurn() ; i++)
		{
			sb.append(I18n.tr(R.string.game_turn,i));

			for (Player p : players_)
			{
				if (p.hasTurn(i))
					sb.append(Share.doubleToString(p.getPoints(i),config_.getDecimalPlace()));
				else
					sb.append("      ");

				for (j=4 ; j < p.getName().length(); j++)
					sb.append(" ");

				sb.append("|");
			}
			sb.append("\n");
		}

		return sb.toString();
	}

	/**
	 * Convert the total points to a string
	 * @return the string
	 */
	private String toStringTotalPoints() {
		StringBuilder sb = new StringBuilder();
		int i;

		sb.append(I18n.tr(R.string.game_total));

		for (Player p : players_)
		{
			sb.append(Share.doubleToString(p.getTotalPoints(),config_.getDecimalPlace()));

			for (i=4 ; i < p.getName().length(); i++)
				sb.append(" ");

			sb.append("|");
		}

		return sb.toString();
	}

	/**
	 * Convert the ranking to a string
	 * @return the string
	 */
	private String toStringRanking() {
		StringBuilder sb = new StringBuilder();
		int i;


		//TRANSLATORS:The number of characters before the | must be eight
		sb.append(I18n.tr(R.string.game_ranking));

		for (Player p : players_)
		{
			sb.append(p.getRanking());

			for (i=4 ; i < p.getName().length(); i++)
				sb.append(" ");

			sb.append("|");
		}

		return sb.toString();
	}

}
