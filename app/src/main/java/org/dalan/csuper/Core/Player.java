/*
* Player.java
*
* Copyright 2014-2016
* Remi BERTHO <remi.bertho@openmailbox.org>
*
* This file is part of Csuper.
*
* Csuper is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* Csuper is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
*
*
*/

package org.dalan.csuper.Core;

import android.os.Parcel;
import android.os.Parcelable;

import org.dalan.csuper.Core.Exception.OutOfRange;
import org.dalan.csuper.Core.Listener.PlayerListener;
import org.dalan.csuper.Core.Utility.I18n;
import org.dalan.csuper.Core.Utility.Share;
import org.dalan.csuper.R;
import org.jdom2.Content;
import org.jdom2.Element;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represent a player
 */
class Player implements Cloneable, Parcelable {
	///////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////// Attributes /////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Name of the player
	 */
	private String name_ = "";

	/**
	 * Total points of the player
	 */
	private  double total_points_ = 0;

	/**
	 * Points of the player
	 */
	private List<Double> points_ = null;

	/**
	 * Ranking of the player
	 */
	private int ranking_ = 1;

	/**
	 * Number of turn of the player
	 */
	private int nb_turn_ = 0;

	/**
	 * This is the list of listeners
	 */
	private List<PlayerListener> listeners_;


	///////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////// Constructor ////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////


	/**
	 * Default constructor
	 */
	public Player(){
		points_ = new ArrayList<>();

		listeners_ = new ArrayList<>();
	}

	/**
	 * Constructor with all intern component
	 * @param name the name
	 * @param ranking the ranking
	 * @param points the points
	 */
	public Player(String name,
	              int ranking,
	              List<Double> points){
		this();

		name_ = name;
		ranking_ = ranking;
		nb_turn_ = points_.size() - 1;


		for (Double d : points){
			points_.add(d);
			total_points_ += d;
		}
	}

	/**
	 * Constructor with a name and the initial points
	 * @param initial_points the initals points
	 * @param name the name
	 */
	public Player(double initial_points, String name){
		this();
		name_ = name;
		points_.add(initial_points);
		total_points_ = initial_points;
	}

	/**
	 * Constructor with the initial points
	 * @param initial_points the initals points
	 */
	public Player(double initial_points){
		this(initial_points,"");
	}

	/**
	 * Constructor with a name and the initial points
	 * @param game_config a game configuration for the decimal places
	 * @param name the name
	 */
	public Player(GameConfiguration game_config, String name){
		this(game_config.getInitialScore(),name);
	}

	/**
	 * Constructor with a name and the initial points
	 * @param game_config a game configuration for the decimal places
	 */
	public Player(GameConfiguration game_config){
		this(game_config,"");
	}

	/**
	 * Constructor from an XML element
	 * @param e the XML element
	 */
	public Player(Element e) {
		this();

		List<Element> childs = e.getChildren();

		Element tmp_element = childs.get(0);
		Content tmp_content = tmp_element.getContent(0);
		name_ = tmp_content.getValue();

		tmp_element = childs.get(1);
		tmp_content = tmp_element.getContent(0);
		total_points_ = Share.stringToDouble(tmp_content.getValue());

		tmp_element = childs.get(2);
		tmp_content = tmp_element.getContent(0);
		ranking_ = Share.stringToInt(tmp_content.getValue());

		tmp_element = childs.get(3);
		tmp_content = tmp_element.getContent(0);
		nb_turn_ = Share.stringToInt(tmp_content.getValue()) - 1;

		tmp_element = childs.get(4);
		List<Element> turns = tmp_element.getChildren();
		for (Element turn : turns){
			tmp_content = turn.getContent(0);
			points_.add(Share.stringToDouble(tmp_content.getValue()));
		}
	}

	/**
	 * Clone the player
	 * @return the clone
	 */
	@Override
	public Player clone() {
		Player p = null;

		try {
			p = (Player) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}

		p.name_ = name_;
		p.total_points_ = total_points_;
		p.nb_turn_ = nb_turn_;
		p.ranking_ = ranking_;
		p.points_ = new ArrayList<>();
		for(Double d : points_)
		p.points_.add(d);

		return p;
	}


	///////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////// Function ///////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Check if two players are equals
	 * @param p another player
	 * @return true if their name are equals, false otherwise
	 */
	public boolean equals(Player p){
		return p.name_.equals(name_);
	}

	/**
	 * Check if the player name is equals to a name
	 * @param s the name
	 * @return true if their name are equals, false otherwise
	 */
	public boolean equals(String s){
		return s.equals(name_);
	}

	/**
	 * Convert the player in a string
	 * @return the string
	 */
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();

		sb.append(I18n.tr(R.string.player_name)).append(" ").append(name_).append("\n");
		sb.append(I18n.tr(R.string.player_nb_turn)).append(" ").append(Share.intToString(nb_turn_)).append("\n");
		sb.append(I18n.tr(R.string.player_ranking)).append(" ").append(Share.intToString(ranking_)).append("\n");
		sb.append(I18n.tr(R.string.player_points_total_points)).append("\n");

		for (int i=0 ; i< nb_turn_+1 ; i++)
		{
			sb.append(I18n.tr(R.string.player_turn))
					.append(" ")
					.append(Share.intToString(i))
					.append("\t")
					.append(Share.doubleToString(getPoints(i)))
					.append("\t")
					.append(Share.doubleToString(getTotalPoints(i)))
					.append("\n");
		}

		return sb.toString();
	}

	/**
	 * Convert the player in a string
	 * @param game_config a game configuration
	 * @return the string
	 */
	public String toString(GameConfiguration game_config){
		int decimal = game_config.getDecimalPlace();

		StringBuilder sb = new StringBuilder();

		sb.append(I18n.tr(R.string.player_name)).append(" ").append(name_).append("\n");
		sb.append(I18n.tr(R.string.player_nb_turn)).append(" ").append(Share.intToString(nb_turn_)).append("\n");
		sb.append(I18n.tr(R.string.player_ranking)).append(" ").append(Share.intToString(ranking_)).append("\n");
		sb.append(I18n.tr(R.string.player_points_total_points)).append("\n");

		for (int i=0 ; i< nb_turn_+1 ; i++)
		{
			sb.append(I18n.tr(R.string.player_turn))
					.append(" ")
					.append(Share.intToString(i))
					.append("\t")
					.append(Share.doubleToString(getPoints(i),decimal))
					.append("\t")
					.append(Share.doubleToString(getTotalPoints(i),decimal))
					.append("\n");
		}

		return sb.toString();
	}

	/**
	 * Calculate the mean points
	 * @return the mean points
	 */
	public double meanPoints(){
		return total_points_ / nb_turn_;
	}

	/**
	 * Indicate if the player has a specific turn
	 * @param turn the turn
	 * @return true if the player has the turn, false otherwise
	 */
	public boolean hasTurn(int turn) {
		return (turn <= nb_turn_);
	}

	/**
	 * Create an XML element based on the player
	 * @return the XML element
	 */
	public Element toXmlElement(){
		Element e = new Element("player");

		e.addContent(new Element("player_name").setText(name_));
		e.addContent(new Element("total_points").setText(Share.doubleToString(total_points_)));
		e.addContent(new Element("rank").setText(Share.intToString(ranking_)));
		e.addContent(new Element("number_of_turn").setText(Share.intToString(nb_turn_)));

		Element points_element = new Element("points");
		for (int i=0 ; i< getNbTurn() ; i++)
		{
			Element tmp_points_element = new Element("turn");
			tmp_points_element.setText(Share.doubleToString(points_.get(i)));
			tmp_points_element.setAttribute("num", Share.intToString(i));
			points_element.addContent(tmp_points_element);
		}
		e.addContent(points_element);


		return e;
	}


	///////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////// Listener ///////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Add a listener to the game configuration
	 * @param l the listener
	 */
	public void addListener(PlayerListener l){
		listeners_.add(l);
	}

	/**
	 * Remove a listener to the game configuration
	 * @param l the listener
	 */
	public void removeListener(PlayerListener l){
		listeners_.remove(l);
	}

	/**
	 * Call onGameChange to all listeners
	 */
	public void sendSignalChange(){
		for (PlayerListener pl : listeners_)
			pl.onPlayerChange();
	}


	///////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////// Getter and setter /////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////
	public String getName() {
		return name_;
	}

	public void setName(String name_) {
		this.name_ = name_;
		sendSignalChange();
	}

	public double getTotalPoints() {
		return total_points_;
	}

	public double getTotalPoints(int turn) throws OutOfRange {
		if (turn < 0 || turn > nb_turn_)
			throw new OutOfRange(name_ + " has only only " + points_.size() + " turn, not " + turn + ".");

		double total = 0;

		for (int i=0 ; i<turn+1 ; i++)
			total += points_.get(i);

		return total;
	}

	public int getRanking() {
		return ranking_;
	}

	public void setRanking(int ranking_) {
		this.ranking_ = ranking_;
		sendSignalChange();
	}

	public int getNbTurn() {
		return nb_turn_;
	}

	public double getPoints(int turn) throws OutOfRange {
		if (turn < 0 || turn > nb_turn_)
			throw new OutOfRange(name_ + " has only only " + points_.size() + " turn, not " + turn + ".");

		return points_.get(turn);
	}

	public double getPoints(){
		return points_.get(points_.size()-1);
	}

	/**
	 * Set the points at a specific turn
	 * @param turn the turn
	 * @param point the points
	 * @throws OutOfRange if turn is greater than the number of turn or smaller than 0
	 */
	public void setPoints(int turn, double point) throws OutOfRange {
		if (turn < 0 || turn > nb_turn_)
			throw new OutOfRange(name_ + " has only only " + points_.size() + " turn, not " + turn + ".");

		double points_diff = points_.get(turn) - point;
		points_.set(turn,point);

		total_points_ -= points_diff;

		sendSignalChange();
	}

	/**
	 * Add points to the player
	 * @param points the points
	 */
	public void addPoints(double points) {
		points_.add(points);
		total_points_ += points;
		nb_turn_++;

		sendSignalChange();
	}

	/**
	 * Delete a turn
	 * @param turn the turn
	 * @throws OutOfRange if turn is greater than the number of turn or smaller than 0
	 */
	public void deleteTurn(int turn) throws OutOfRange {
		if (turn < 0 || turn > nb_turn_)
			throw new OutOfRange(name_ + " has only only " + points_.size() + " turn, not " + turn + ".");

		total_points_ -= points_.get(turn);
		points_.remove(turn);
		nb_turn_--;

		sendSignalChange();
	}


	///////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////// Parcelable /////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(ranking_);
		dest.writeInt(nb_turn_);
		dest.writeDouble(total_points_);
		dest.writeString(name_);
		dest.writeList(points_);
	}

	public static final Parcelable.Creator<Player> CREATOR = new Parcelable.Creator<Player>(){

		@Override
		public Player createFromParcel(Parcel source) {
			return new Player(source);
		}

		@Override
		public Player[] newArray(int size) {
			return new Player[size];
		}
	};

	public Player(Parcel in) {
		this();
		ranking_ = in.readInt();
		nb_turn_ = in.readInt();
		total_points_ = in.readDouble();
		name_ = in.readString();
		in.readList(points_,null);
	}
}
