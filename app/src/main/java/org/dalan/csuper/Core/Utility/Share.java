/*
* Share.java
*
* Copyright 2014-2016
* Remi BERTHO <remi.bertho@openmailbox.org>
*
* This file is part of Csuper.
*
* Csuper is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* Csuper is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
*
*
*/

package org.dalan.csuper.Core.Utility;

import org.dalan.csuper.Core.Utility.I18n;
import org.dalan.csuper.R;

import java.text.DecimalFormat;

/**
 * This class provide some usefull shared function
 * @author Rémi BERTHO
 */
public class Share {
	/**
	 * Convert a boolean to a translated string
	 * @param b a boolean
	 * @return the string
	 */
	public static String boolToStringTr(boolean b){
		if (b)
			return I18n.tr(R.string.True);
		else
			return I18n.tr(R.string.False);
	}

	/**
	 * Convert a boolean to a string
	 * @param b a boolean
	 * @return "yes" if b is true "no" otherwise
	 */
	public static String boolToString(boolean b){
		if (b)
			return "yes";
		else
			return "no";
	}

	/**
	 * Convert a string to a boolean
	 * @param s the string
	 * @return true id s is "yes", false otherwise
	 */
	public static boolean stringToBool(String s){
		if (s.equals("yes"))
			return true;
		else
			return false;
	}

	/**
	 * Convert an int to a string
	 * @param i the int
	 * @return the string
	 */
	public static String intToString(int i){
		Integer j = i;
		return j.toString();
	}

	/**
	 * Convert a string to an int
	 * @param s the string
	 * @return the int
	 */
	public  static int stringToInt(String s){
		return Integer.parseInt(s);
	}

	/**
	 * Convert a double to a string with a number of decimals
	 * @param d the double
	 * @param decimals the number of decimals
	 * @return the string
	 */
	public static String doubleToString(double d, int decimals){
		if ((new Double(d)).isInfinite())
			return "inf";
		DecimalFormat df = null;
		switch (decimals){
			case 0:
				df = new DecimalFormat("#");
				break;
			case 1:
				df = new DecimalFormat("#.#");
				break;
			case 2:
				df = new DecimalFormat("#.##");
				break;
			case 3:
				df = new DecimalFormat("#.###");
				break;
			case 4:
				df = new DecimalFormat("#.####");
				break;
			case 5:
				df = new DecimalFormat("#.#####");
				break;
			case 6:
				df = new DecimalFormat("#.######");
				break;
			case 7:
				df = new DecimalFormat("#.#######");
				break;
			case 8:
				df = new DecimalFormat("#.########");
				break;
			case 9:
				df = new DecimalFormat("#.#########");
				break;
			case 10:
				df = new DecimalFormat("#.##########");
				break;
		}
		return df.format(d);
	}

	/**
	 * Convert a double to a string
	 * @param d the double
	 * @return the string
	 */
	public static String doubleToString(double d){
		Double e = d;
		if (e.isInfinite())
			return "inf";

		return e.toString();
	}

	/**
	 * Convert a string to a double
	 * @param s the string
	 * @return the double
	 */
	public static double stringToDouble(String s){
		if (s.equals("inf"))
			return Double.POSITIVE_INFINITY;
		return Double.parseDouble(s);
	}
}
