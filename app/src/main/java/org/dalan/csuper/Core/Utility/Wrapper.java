/*
* Wrapper.java
*
* Copyright 2014-2016
* Remi BERTHO <remi.bertho@openmailbox.org>
*
* This file is part of Csuper.
*
* Csuper is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* Csuper is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
*
*
*/

package org.dalan.csuper.Core.Utility;

/**
 * Wrapp a class to permit to change a value by passing in a function
 */
public class Wrapper<E> {
	/**
	 * The value wrapped
	 */
	E ref;

	/**
	 * Constructor
	 * @param e the wrapped value
	 */
	public Wrapper( E e ){
		ref = e;
	}

	/**
	 * Get the value
	 * @return the value
	 */
	public E get() {
		return ref;
	}

	/**
	 * Set the value
	 * @param e the value
	 */
	public void set( E e ){
		this.ref = e;
	}
}
